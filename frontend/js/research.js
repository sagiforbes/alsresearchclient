var app = angular.module('researchApp', ['ngMaterial','ngRoute'])
.run(function($rootScope) {
    $rootScope.allData=[];
});

app.filter('genderToText',function(){
    return function(input){
        if(input==0)
        {
            return 'Male';
        }
        if(input ==1) {
            return 'Female'
        }
        return 'Unknown';
    }

});

 // configure our routes
app.config(function($routeProvider) {


    $routeProvider
    .when('/', {
        templateUrl : 'views/patientlist.html',
        controller  : 'patientListController'
    })
    .when('/detail',{
        templateUrl : 'views/patientdetails.html',
        controller  : 'patientDetailsController'
      });

});






app.controller("researchController",function($scope,$rootScope){

});


app.controller("patientDetailsController",function($scope,$rootScope,$location,$http,$window){
    var scp=$scope;
    scp.patient={};

    scp.pageInit=function() {
        var requestInfo=$location.search();

        if(typeof requestInfo ==='undefined')
        {
            console.log("Unknown patient");
            scp.gotoList();
            return;
        }

        if(typeof requestInfo.id ==='undefined')
        {
            console.log("Unknown patient");
            scp.gotoList();
            return;
        }


        scp.patient=null;
        for(var idx=0;idx<$rootScope.allData.length;idx++) {
            var curData=$rootScope.allData[idx];
            if(curData.patientID.localeCompare(requestInfo.id)==0) {
                scp.patient=curData;
                break;
            }
        }

        if(scp.patient === null)
        {
              console.log("Unknown patient");
              scp.gotoList();
              return;
        }

    }


    scp.gotoList=function(){
        $location.path("/");
    }


});


app.controller("patientListController",function($scope,$rootScope,$http,$location,$window) {


    var scp=$scope;

    scp.baseUrl="https://alsdevprize4life.hana.ondemand.com/als";


    scp.filterCol=$rootScope.allData;;


    scp.ageRange=[
        {"key":0,"val":"All"},
        {"key":1,"val":"0-20"},
        {"key":2,"val":"20-30"},
        {"key":3,"val":"30-40"},
        {"key":4,"val":"40-50"},
        {"key":5,"val":"50-60"}
    ];

    scp.genderRange=[
               {"key":0,"val":"Male & Female"},
               {"key":1,"val":"Male"},
               {"key":2,"val":"Female"}
           ];


    scp.patientTypeRange=[
        {"key":"All","val": "All"},
        {"key":"PATIENT","val": "PATIENT"},
        {"key":"CONTROL","val": "CONTROL"}
    ];


    scp.fromMonthRange=[];
    scp.toMonthRange=[];

    scp.ageFilterVal=0;
    scp.genderFilterVal=0;
    scp.fromMonthFilterVal=0;
    scp.toMonthFilterVal=0;
    scp.patientFilterVal="All";


    scp.showDetails = function(patientId) {
        $location.path("/detail").search({"id":patientId});
    }

    scp.getAndwers = function() {
                $http({"method":"POST","url":scp.baseUrl+"/j_spring_security_check?j_username=als_analyzer@prize4life.org&j_password=Welcome1"}).then(function(o){
                    $http({"method":"GET","url":scp.baseUrl+"/api/tasks/questionnaires", headers: {
                    "Content-Type": "application/json",
                    "Authorization":"Basic YWxzX2FuYWx5emVyQHByaXplNGxpZmUub3JnOldlbGNvbWUx"
                    }
                     }).then(function(o){
                            scp.buildData(o.data);
                        },function(err){
                            //$window.alert("Failed to get info from server "+err.status);
                            scp.buildData(offlineQuestionier);

                        });
                },function(err){
                       $window.alert(err.data);
                });

        };

        scp.buildData=function(rawDataCol){

            var today = new Date().getTime();
            var map=[];
            for(var rawIndex=0;rawIndex<rawDataCol.length;rawIndex++) {
                var rawData=rawDataCol[rawIndex];
                var patientId=rawData.patientId.toString();
                try {

                    if(typeof map[patientId] === 'undefined')       //creating new patient entry
                    {
                        map[patientId] = {};
                        map[patientId]['raw'] = [];
                        map[patientId]['patientID'] = patientId;
                        map[patientId]['gender'] = rawData.patientDetails.gender;
                        map[patientId]['birthday'] = rawData.patientDetails.birthday;
                        map[patientId]['age'] = Math.floor((today - new Date(rawData.patientDetails.birthday).getTime())/(1000*60*60*24*365));
                        map[patientId]['diagnoseDate'] = rawData.patientDetails.diagnoseDate;
                        if(typeof rawData.patientDetails.diagnoseDate ==='undefined') {
                            map[patientId]['diagnoseSpan']="";
                            map[patientId]['diagnoseAge']="";
                        }
                        else
                        {
                            map[patientId]['diagnoseSpan'] = Math.floor(( today - new Date(rawData.patientDetails.diagnoseDate).getTime())/(1000*60*60*24*30));
                            map[patientId]['diagnoseAge']= Math.floor(( new Date(rawData.patientDetails.diagnoseDate).getTime() - new Date(rawData.patientDetails.birthday).getTime())/(1000*60*60*24*365));
                        }

                        map[patientId]['userType'] = rawData.patientDetails.userType;
                        map[patientId]['lastQScore']=0;
                        map[patientId]['lastQSize']=0;
                        map[patientId]['allQ']={};

                    }

                    map[patientId]['lastQDate']=rawData.created;

                    map[patientId].raw.push(rawData);

                    var lastQScore=0;
                    map[patientId].lastQSize=0;
                    if(rawData.answers.length>0){
                        for(var answIdx=0;answIdx<rawData.answers.length;answIdx++) {
                            var answer=rawData.answers[answIdx];
                            if(typeof answer.answer !== 'undefined') {
                                map[patientId].lastQSize++;
                                lastQScore+=answer.answer;
                                var allQ=map[patientId].allQ;

                                if(typeof allQ[answer.questionName]==='undefined'){
                                    allQ[answer.questionName]=[];
                                }

                                allQ[answer.questionName].push({"date":rawData.created,"score":answer.answer,"remark":answer.remark});
                            }
                        }


                    }

                    map[patientId].lastQScore=lastQScore;


                }
                catch (err) {
                    console.log('Failed to analyze record '+rawData.id+". "+err.message);
                }

            }

            $rootScope.allData=[];
            for(var key in map) {
                var data=map[key];
                $rootScope.allData.push(data);
            }

            scp.filterCol=$rootScope.allData;

        };

    scp.filterByAge=function (allRec) {
        if(scp.ageFilterVal==0) {
            return allRec;
        }

        var fromAge=0;
        var toAge=60;

        if(scp.ageFilterVal==1) {
            fromAge=0;
            toAge=20;
        }
        else {
            fromAge=scp.ageFilterVal*10;
            toAge=fromAge+10;
        }

        var ret=[];

        for(var idx=0 ; idx< allRec.length;idx++) {
            var data=allRec[idx];
            if(data.age>=fromAge && data.age < toAge) {
                ret.push(data);
            }

        }

        return ret;

    };

    scp.filterByGender =function(allRec) {
        if(scp.genderFilterVal==0) {
            return allRec;
        }

        var expGen=0;

        if(scp.genderFilterVal == 2) {
            expGen=1;
        }

        var ret=[];
         for(var idx=0; idx< allRec.length;idx++) {
             var data=allRec[idx];
             if(data.gender==expGen) {
                 ret.push(data);
             }

         }
         return ret;
    }

    scp.filterByDiagnostics=function(allRec) {
        if(scp.fromMonthFilterVal==0 && scp.toMonthFilterVal == 0) {
            return allRec;
        }

        var fromDiag=scp.fromMonthFilterVal;
        var toDiag=scp.toMonthFilterVal;

        if(fromDiag>toDiag) {
            var tmp=fromDiag;
            fromDiag=toDiag;
            toDiag=tmp;
        }

        if(fromDiag==toDiag)
            toDiag=fromDiag+1;

        var ret=[];
        for(var idx=0; idx< allRec.length;idx++) {
             var data=allRec[idx];
             if(data.diagnoseSpan>=fromDiag && data.diagnoseSpan<toDiag) {
                 ret.push(data);
             }

        }

        return ret;
    };

    scp.filterByPatient=function(allRec) {
        if(scp.patientFilterVal.localeCompare("All")==0) {
            return allRec;
        }


        var ret=[];
         for(var idx=0; idx< allRec.length;idx++) {
             var data=allRec[idx];
             if(data.userType.localeCompare(scp.patientFilterVal)==0) {
                 ret.push(data);
             }

         }
         return ret;
    };

    scp.runFilter = function() {
        var tmpFilter = $rootScope.allData;
        tmpFilter = scp.filterByAge(tmpFilter);
        tmpFilter = scp.filterByGender(tmpFilter);
        tmpFilter = scp.filterByDiagnostics(tmpFilter);
        tmpFilter = scp.filterByPatient(tmpFilter);

        scp.filterCol = tmpFilter;
    };



    scp.resetFilter = function() {
        scp.ageFilterVal = 0;
        scp.genderFilterVal = 0;
        scp.fromMonthFilterVal = 0;
        scp.toMonthFilterVal = 0;
        scp.patientFilterVal="All";

        scp.runFilter();
    };

    scp.pageInit=function() {

        scp.getAndwers();


        for(var i=0;i<12*6;i++)
        {
            scp.fromMonthRange.push(i);
        }


        for(var i=0;i<(12*6)+1;i++)
        {
            scp.toMonthRange.push(i);
        }

        scp.resetFilter();
    }



    scp.onSelectFromDiagMonth=function() {
        if(scp.fromMonthFilterVal > scp.toMonthFilterVal) {
            scp.toMonthFilterVal=scp.fromMonthFilterVal+1;
        }
    }

    scp.onSelectToDiagMonth = function(){
        if(scp.toMonthFilterVal<scp.fromMonthFilterVal) {
            if(scp.toMonthFilterVal>0) {
               scp.fromMonthFilterVal = scp.toMonthFilterVal-1;
            }
            else {
               scp.fromMonthFilterVal=0;
            }
        }


    };



});

